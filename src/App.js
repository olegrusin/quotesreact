import React, { Component } from "react";
import "./App.css";
import quotesData from "./quotesData";


class Quote extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <p id="text">"{this.props.quote}"</p>
        <p id="author">{this.props.author}</p>
        <div className='buttons'>
          <a id="tweet-quote" href={"https://twitter.com/intent/tweet?text="+this.props.quote+" - "+this.props.author} target="_blank" rel="noopener noreferrer">tweet</a>
          <button id="new-quote" onClick={this.props.getQuote}>New</button>
        </div>
      </div>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      quote: "",
      author: ""
    };
  }

  componentDidMount() {
    this.setState({
      data: quotesData,
      quote: quotesData[Math.floor(Math.random() * quotesData.length)].quote,
      author: quotesData[Math.floor(Math.random() * quotesData.length)].author
    });
  }

  getQuote = () => {
    let random = this.state.data[Math.floor(Math.random() * this.state.data.length)];
    this.setState({
      quote: random.quote,
      author: random.author
    })
  }

  render() {
    return (
      <div id="quote-box">
        <Quote
          quote={this.state.quote}
          author={this.state.author}
          getQuote={this.getQuote}
        />
      </div>
    );
  }
}
export default App;
